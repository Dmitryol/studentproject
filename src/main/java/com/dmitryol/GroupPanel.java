package com.dmitryol;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
/**
 * 
 * @author Dmitryol
 *
 */

class GroupPanel extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private JTextField textfield = new JTextField();
	private JLabel add_group = new JLabel("Add Group");
	private JLabel title = new JLabel("Title group:");
	private JButton button = new JButton();
	
	private GroupModel gm = new GroupModel();
	private JTable table = new JTable(gm);
	/**
	 * Constructor of this class
	 */
	public GroupPanel(){
		
		setLayout(null);
		
		Font font = new Font("Arial", Font.BOLD,20);
		//Table setting
		JScrollPane jsp = new JScrollPane(table);
		RowSorter<TableModel> sort = new TableRowSorter<TableModel>(gm);
		table.setPreferredScrollableViewportSize(new Dimension(540, 120));
		table.setFillsViewportHeight(true);
		table.setCellSelectionEnabled(true);
		table.setRowSorter(sort);
		jsp.setBounds(10, 120, 570, 200);
		
		button.addActionListener(this);
		//Configure components
		textfield.setBounds(100, 60, 200, 25);
		add_group.setBounds(20, 0, 200, 50);
		title.setBounds(10, 60, 100, 25);
		button.setBounds(320, 60, 70, 25);	
		add_group.setFont(font);
		button.setText("Add");
		
		//Adding components
		add(add_group);
		add(textfield);
		add(title);
		add(button);
		add(jsp);
	}

	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource()==button){
			gm.addGruop(textfield.getText());
			textfield.setText("");
			table.repaint();
		}
		
	}
}
