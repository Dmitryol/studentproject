package com.dmitryol;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
/**
 * 
 * @author Dmitryol
 *
 */

class ExamsPanel extends JPanel implements ActionListener   {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JTextField textfield = new JTextField();
	private JComboBox<String> year = new JComboBox<String>(getYear()); 
	private JComboBox<String> month = new JComboBox<String>(getMonth()); 
	private JComboBox<String> day = new JComboBox<String>(getDay()); 
	private JButton button = new JButton();
	private JTable table;
	private ExamModel exam_model = new ExamModel();
	/**
	 * Constructor of this class
	 */
	public ExamsPanel(){
	
		setLayout(null);
		
		table=new JTable(exam_model);
		
		//Setting table
		RowSorter<TableModel> sort = new TableRowSorter<TableModel>(exam_model);
		JScrollPane jsp=new JScrollPane(table);
		table.setPreferredScrollableViewportSize(new Dimension(555, 180));
		table.setFillsViewportHeight(true);
		table.setCellSelectionEnabled(true);
		table.setRowSorter(sort);
		jsp.setBounds(10, 150, 550, 180);
		
		//Setting panel
		textfield.setBounds(100, 50, 200, 25);
		year.setBounds(100, 100, 100, 20);
		month.setBounds(220, 100, 50, 20);
		day.setBounds(300, 100, 50, 20);
		button.setText("Add");
		button.setBounds(400, 95, 100, 25);
	    button.addActionListener(this);	

	    //Adding components
		add(textfield);
		add(year);
		add(month);
		add(day);
		add(button);
		add(jsp);
		add(setLabel("Exam", 20, 50));
		add(setLabel("Date", 20, 95));
		add(setLabel("Year", 105, 75));
		add(setLabel("Month", 225, 75));
		add(setLabel("Day",305, 75));
		Font font = new Font("Arial", Font.BOLD,20);
		add(setLabel("Create Exam", 20, 10)).setFont(font);
		
		
		
		
	}
	private JLabel setLabel(String title, int x, int y){
		JLabel label = new JLabel(title);
		label.setBounds(x, y, 200, 25);
		return label;
	}
	private String[] getYear(){
		String[] years = new String[24];
	    int year = 2017;
		for(int i=0; i<years.length; i++){
			years[i]= ""+year+"";
			year++;
		}
		return years;
	}
	private String[] getMonth(){
		String[] months = new String[12];
	    int month =1;
		for(int i=0; i<months.length; i++){
			months[i]= ""+month+"";
			month++;
		}
		return months;
	}
	private String[] getDay(){
		String[] days = new String[31];
	    int day =1;
		for(int i=0; i<days.length; i++){
			days[i]= ""+day+"";
			day++;
		}
		return days;
	}
	private String getDate(){
		return year.getSelectedItem()+"-"+month.getSelectedItem()+"-"+day.getSelectedItem();
	}
	/**
	 * This method handles the press of a button
	 */
	public void actionPerformed(ActionEvent event) {
		try{
			if(event.getSource()==button){
				exam_model.addExam(textfield.getText(), getDate());
			    textfield.setText("");
				repaint();
			}
			
		}catch(Exception e){
			
		}
		
	}

}
