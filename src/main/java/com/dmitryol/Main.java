package com.dmitryol;

import javax.swing.JFrame;

/**
 * 
 * @author Dmitryol
 *
 */

public class Main extends JFrame {
	
	private static final long serialVersionUID = -7876286676846283468L;

	protected static int width = 600;
	protected static int height = 400;
	 /**
	  * setting window
	  */
	public Main(){
		setTitle("MyProject");
		setSize(width, height);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(true);
		
	}
/**
 * 
 * @param args
 */
	public static void main(String[] args) {
	    Main frame=new Main();
	    frame.setLayout(null);
	    Tab tab = new Tab(); 
	    frame.add(tab);
	    frame.repaint();
	}
	
}
