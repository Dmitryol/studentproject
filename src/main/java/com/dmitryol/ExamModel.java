package com.dmitryol;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;
/**
 * 
 * @author Dmityol
 *
 */
class ExamModel extends AbstractTableModel {
	
	
	private static final long serialVersionUID = 1L;
	private ArrayList<String[]> exams;
	private int columnCount  = 2;
	private DatabaseReader db = new DatabaseReader();
	/**
	 * Constructor of this class
	 */
	public ExamModel(){
		exams = new ArrayList<String[]>();
		examUpdate();
	}
    /**
     * This method returns the count columns of the table
     */
	public int getColumnCount() {
		return columnCount;
	}
    /**
     * This method returns the count row of the table
     */
	public int getRowCount() {
		return exams.size();
	}
    /**
     * This method returns the value at the specified column and row
     */
	public Object getValueAt(int rowIndex, int columnIndex) {
		String[]row=exams.get(rowIndex);
		return row[columnIndex];
		
	}
	/**
	 * 
	 */
	public String getColumnName(int columnIndex){
		switch(columnIndex){
		case 0: return "SUBJECT";
		case 1: return "DATE";
		}
		return "";

}
	public void examUpdate(){
		exams = db.tableUpdate("select subject, date from exams", columnCount);
		
	}
	/**
	 * This method adds exam in the database
	 * 
	 * @param subject - Name of the exam
	 * @param date - Date of the exam
	 */
	public void addExam(String subject, String date){
		
		db.tableEdit("insert into exams (subject, date) values ('"+subject+"', '"+date+"');");
        examUpdate();
      
	}
	
}
