package com.dmitryol;

import java.beans.PropertyVetoException;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * 
 * @author Dmityol
 *
 */
class DatabaseReader {
	/**
	 * Path to file properties
	 */
	private static final String Path ="src/main/resources/database.properties";
	private Properties configData;
	private ComboPooledDataSource pool= new ComboPooledDataSource();
	
	/**
	 * Constructor loads driver for postgres 
	 */
	public DatabaseReader(){	
		getProperties();
		setPooled(1, 20, 1);
		
	}
	/**
	 * Method gets data from properties file
	 */
	private void getProperties(){
		try {
			FileInputStream config = new FileInputStream(Path);	
		    configData = new Properties();
			configData.load(config);
		}
		 catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	/**
               * Setting of the pooled
               */

	private void setPooled(int minPool, int maxPool, int acquireIncrement){
		try {
			pool.setDriverClass(configData.getProperty("driver"));
			pool.setJdbcUrl(configData.getProperty("url"));
			pool.setUser(configData.getProperty("username"));
			pool.setPassword(configData.getProperty("password"));
			pool.setMinPoolSize(minPool);
			pool.setAcquireIncrement(acquireIncrement);
			pool.setMaxPoolSize(maxPool);
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 *  The method receives the SQL query for update table and returns data in the form ArrayList String arrays	
	 * @param sql - SQL command for update table   
	 * @param columnCount - amount columns in table
	 * @return The data array form table
	 */
	public ArrayList<String[]> tableUpdate (String sql, int columnCount){
				
		ArrayList<String[]> data = new ArrayList<String[]>();
			
		try {
			Connection con = pool.getConnection();
			
			Statement st=con.createStatement();
			ResultSet result=st.executeQuery(sql);
			
			while(result.next()){
				String[]row=new String[columnCount];
				for(int i=0; i<row.length; i++){
				row[i]=result.getString(i+1);}
				data.add(row);
			}
			st.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return data;
	}
	/**
	 * The method receives the SQL query to change data in the table
	 * @param sql - SQL query
	 */
	public void tableEdit (String sql){

	try{
		Connection con = pool.getConnection();
		Statement st=con.createStatement();
		st.execute(sql);
		st.close();
		con.close();
	}catch(SQLException e){
		e.printStackTrace();
	}
	}
	/**
	 * The method receives the SQL query to retrieve data from table of the groups and returns data in the form ArrayList of the Strings 
	 * @param sql - SQL query for get data from table of the groups  
	 * @return ArrayList of the Strings
	 */
	public ArrayList<String> groupsUpdate(String sql){
		
		ArrayList<String> groupsList = new ArrayList<String>();
		try{
			Connection con = pool.getConnection();
			Statement st=con.createStatement();
		    ResultSet result=st.executeQuery(sql);
		    while(result.next()){
		    	groupsList.add(result.getString(1));
		    }
		    st.close();
			con.close();
			
			return groupsList;
			
		}catch(SQLException e){
			e.printStackTrace();
			
			return groupsList;
		}
	}
	/**
	 * The method returns the ID value of a certain group
	 * @param sql - SQL query to get value of the group
	 * @return ID value of the group
	 */
	public int getNumber(String sql){
	    
		try{
			int number_group=1;
			Connection con = pool.getConnection();
			Statement st=con.createStatement();
		    ResultSet result=st.executeQuery(sql);
		    while(result.next()){	   
		    number_group=Integer.parseInt(result.getString(1));}
		    st.close();
			con.close();
			return number_group;
			
		}catch(SQLException e){
			e.printStackTrace();
			return 0;
			
			
		}
	}

	
	
}

