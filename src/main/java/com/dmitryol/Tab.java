package com.dmitryol;

import javax.swing.JTabbedPane;
/**
 * 
 * @author Dmitryol
 *
 */
class Tab extends JTabbedPane {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4782201828797205149L;
	
		
	public Tab(){
		StudentsPanel stud_panel =new StudentsPanel();
		GroupPanel group_panel = new GroupPanel();
		ExamsPanel exam_panel = new ExamsPanel();
		AttendedStudentsPanel asp = new AttendedStudentsPanel();
		PassedStudentsPanel psp = new PassedStudentsPanel();
		setSize(Main.width, Main.width);
		addTab("Students", stud_panel);
		addTab("Groups", group_panel);
		addTab("Exams", exam_panel);
		addTab("AttendedStudents", asp);
		addTab("PassedStudents", psp);
	
	
	}

}
