package com.dmitryol;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
/**
 * This class is model for table of students
 * 
 * @author Dmitryol
 *
 */
class StudentTableModel extends AbstractTableModel{
	

	private static final long serialVersionUID = -2915784194058002245L;
	
	private ArrayList<String[]> studArray;
	private int columnCount=3;
	private DatabaseReader dataReader;
	/**
	 * This constructor accepts data form database and adds in the table
	 */	
	public StudentTableModel(){
		dataReader= new DatabaseReader();
		studArray=new ArrayList<String[]>();
		studentsUpdate();
	}

	public int getColumnCount() {
		return columnCount;
	}

	public int getRowCount() {
		return studArray.size();
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		String[]row=studArray.get(rowIndex);
		return row[columnIndex];
	}
	/**
	 * Method adding title of column for table
	 */
	@Override
	public String getColumnName(int columnIndex){
		switch(columnIndex){
		case 0: return "NAME";
		case 1: return "SURMANE";
		case 2: return "GROUP";
		}
		return "";
	}
	/**
	 * This method adds student in the database  
	 * @param name - name of student
	 * @param surname - surname of student
	 * @param group - group for student
	 */
	public void addStudents(String name, String surname, String group){
	    dataReader.tableEdit("insert into students (name, surname, number_group) values ('"+name+"', '"+surname+"', '"+group+"')");
		studentsUpdate();
	}
	/**
	 * This method accepts data about students from database 
	 */
	private void studentsUpdate(){
		studArray=dataReader.tableUpdate("select name, surname, title from students s inner join groups g on s.number_group=g.id;", columnCount);
	}
	/**
	 * This method accepts data groups for students  
	 * @return - array Strings of Groups
	 */
	public String[] groupsUpdate(){
		ArrayList<String> dataGroups=new ArrayList<String>();
		dataGroups=dataReader.groupsUpdate("select title from groups; ");
		String[]arrayGroups=new String[dataGroups.size()];;
		for(int i=0;i<dataGroups.size();i++){
			arrayGroups[i]=dataGroups.get(i);
		}
		return arrayGroups;
	}
	/**
	 * The method returns the ID value of a certain group
	 * @param group_title - The title group 
	 * @return The value ID of a certain group
	 */
	public int getNamerGroup(String group_title){
		String sql="select id from groups where title='"+group_title+"';";
		int i=dataReader.getNumber(sql);
		return i;
		
		
		
	}

}

