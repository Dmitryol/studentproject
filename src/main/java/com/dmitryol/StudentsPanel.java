package com.dmitryol;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 * 
 * @author Dmitryol
 *
 */

class StudentsPanel extends JPanel {
	
	/**
	 * Creating components
	 */
	private static final long serialVersionUID = -5659921738926867589L;
	private JTextField name = new JTextField();
	private JTextField surname = new JTextField();
	private JButton add = new JButton();
	private JTable t;
	private StudentTableModel stm;
	private JComboBox<String> group;
	
	/**
	 * Adding and setting components and table
	 */
	public StudentsPanel(){
		
		
		
		Action act = new Action();
		
		setLayout(null);
				
		stm= new StudentTableModel();
		t=new JTable(stm);
		group = new JComboBox<String>(stm.groupsUpdate());
		RowSorter<TableModel> sort = new TableRowSorter<TableModel>(stm);
		
		// setting table
		JScrollPane jsp=new JScrollPane(t);
		t.setPreferredScrollableViewportSize(new Dimension(540, 120));
		t.setFillsViewportHeight(true);
		t.setCellSelectionEnabled(true);
	    t.setRowSorter(sort);
		jsp.setBounds(10, 200, 540, 120);
		
		// adding components
		componentLocation();
		add.addActionListener(act);
		add(jsp);
		add.setText("Add");	
		add(add);
		add(name);
		add(surname);
		add(group);
		add(label("First Name:", 10, 50));
		add(label("Last  Name:", 10, 80));
		Font font = new Font("Arial", Font.BOLD,20);
		add(label("Create student",20, 10)).setFont(font);
		
		
	}

	private JLabel label(String title, int x, int y){
		JLabel label = new JLabel(title);
		label.setSize(200, 25);
		label.setLocation(x, y);
		return label;
	}
	/**
	 * Method placement components
	 */
	private void componentLocation(){
		add.setBounds(100, 150, 100, 25);
		name.setBounds(100, 50, 200, 25);
		surname.setBounds(100, 80, 200, 25);
		group.setBounds(100, 120, 150, 20);
	
	}
/**
 * Creating listener for button of the add
 */
	private class Action implements ActionListener{

		public void actionPerformed(ActionEvent event) {
			try{
				if(event.getSource()==add){	
		
					stm.addStudents(name.getText(), surname.getText(), ""+stm.getNamerGroup(group.getSelectedItem().toString()));
					name.setText("");
				    surname.setText("");
					t.repaint();
				}
				
				
				
			}catch(Exception e){
				
			}
		}
		
	}

}

