package com.dmitryol;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
/**
 * 
 * @author Dmitryol
 *
 */

class GroupModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<String> data;
	private DatabaseReader database;
	private static final int columnCount=1;
	/**
	 * Constructor of this class
	 */
	public GroupModel(){
		data = new ArrayList<String>();
		database = new DatabaseReader();
		getData();
	}
	/**
	 * Method gets data from the database 
	 */
	public void getData(){
		data = database.groupsUpdate("select title from groups");
	}
	/**
	 * Method adds group in the database
	 * @param title - Title of the group
	 */
	public void addGruop(String title){
		database.tableEdit("insert into groups (title) values ('"+title+"')" );
		getData();
	}
	
	
	public int getColumnCount() {
		return columnCount;
	}

	public int getRowCount() {
		return data.size();
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		return data.get(rowIndex);
	}
	public String getColumnName(int columnIndex){
		switch(columnIndex){
		case 0: return "GOUP NUMBER";
	
		}
		return "";
	}

}
