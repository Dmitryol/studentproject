create table students(
id int not null default nextval('stud_increment') primary key,
name varchar(15) not null,
surname varchar(20) not null,
number_group int references groups(id)); 