# README
 
    Follow steps below to start

  * Download and Install PostgreSQl from oficcial [site](http://www.enterprisedb.com/products-services-training/pgdownload) for your operation system.

  * Open PostgreSQl and create database "test" with user "postgres" and password "qwest123".

  * Clone project

```
#!java

git clone https://Dmitriyol@bitbucket.org/Dmitriyol/studentproject.git
```
  * Import projcet in IDE of your choise

  * build using command
```
#!java

mvn clean package
```

  * Add libraries:  postgresql.jar, c3p0-0.9.5.2.jar, c3p0-oracle-thin-extras-0.9.5.2.jar, mchange-commons-java-0.2.11.jar and liquibase.jar from folder "res" 
  * In folder "res" execute command:
```
#!java

java -jar liquibase.jar update
```
   The command creates tables of databases for project